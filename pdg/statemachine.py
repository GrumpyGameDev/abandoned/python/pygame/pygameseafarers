import pygame, sys
class Machine:
    def __init__(self,current,states):
        self.current = current
        self.states = states
        state = self.get_state(self.current)
        if state!=None:
            state.start()
    def set_current(self,current):
        if current!=self.current:
            state = self.get_state(self.current)
            if state!=None:
                state.finish()
            self.current=current
            state = self.get_state(self.current)
            if state!=None:
                state.start()
    def get_state(self,state_id):
        if state_id in self.states:
            return self.states[state_id]
        else:
            return None
    def draw(self):
        state = self.get_state(self.current)
        if state!=None:
            state.draw()
    def handle(self,event):
        state = self.get_state(self.current)
        if state!=None:
            return state.handle(event)
        else:
            return None

class State:
    def __init__(self):
        return
    def start(self):
        return
    def finish(self):
        return
    def draw(self):
        return
    def handle(self,event):
        return None

class CommonState(State):
    def set_context(self,context):
        self.context=context
    def get_context(self):
        return self.context
    def __init__(self,context):
        State.__init__(self)
        self.set_context(context)

def run_machine(machine):
    while machine.current!=None:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: 
                sys.exit()
            else:
                machine.set_current(machine.handle(event))
        machine.draw()
        pygame.display.flip()

