import random
class Walker:
    def __init__ (self,count):
        self.count = count
        self.values = range(count)
    def get_opposite(self,direction):
        return (direction + self.count//2) % self.count
class CardinalWalker(Walker):
    def __init__(self):
        Walker.__init__(self,4)
        self.delta_x=[0,1,0,-1]
        self.delta_y=[-1,0,1,0]
    def get_next_x(self,direction,x,y):
        return x + self.delta_x[direction % self.count]
    def get_next_y(self,direction,x,y):
        return y + self.delta_y[direction % self.count]
class MazeDoor:
    def __init__(self):
        self.open=False
    def clear(self):
        self.open=False
class MazeCell:
    def __init__(self):
        self.neighbors={}
        self.doors={}
        self.state="outside"
    def set_neighbor(self, direction, neighbor):
        self.neighbors[direction]=neighbor
    def get_neighbor(self, direction):
        if direction in self.neighbors:
            return self.neighbors[direction]
        else:
            return None    
    def set_door(self,direction, door):
        self.doors[direction]=door
    def get_door(self,direction):
        if direction in self.doors:
            return self.doors[direction]
        else:
            return None
    def clear(self):
        for k in self.doors:
            door = self.doors[k]
            door.clear()
        self.state="outside"
    def frontierizeNeighbors(self,frontier):
        for k in self.neighbors:
            next_cell=self.neighbors[k]
            if next_cell.state=="outside":
                frontier.append(next_cell)
                next_cell.state="frontier"
class Maze:
    def __init__(self,columns,rows,walker):
        self.columns = columns
        self.rows    = rows
        self.walker  = walker
        self.cells   = []
        while len(self.cells)<self.columns:
            mazeColumn = []
            self.cells.append(mazeColumn)
            while len(mazeColumn)<self.rows:
                mazeColumn.append(MazeCell())
        for column in range(self.columns):
            for row in range(self.rows):
                cell = self.cells[column][row]
                cell.column=column
                cell.row = row
                for direction in self.walker.values:
                    next_column = self.walker.get_next_x(direction,column,row)
                    next_row = self.walker.get_next_y(direction,column,row)
                    if (direction not in cell.doors) and next_column in range(self.columns) and next_row in range(self.rows):
                        next_cell = self.cells[next_column][next_row]
                        opposite = self.walker.get_opposite(direction)
                        door = MazeDoor()
                        cell.set_neighbor(direction,next_cell)
                        next_cell.set_neighbor(opposite, cell)
                        cell.set_door(direction,door)
                        next_cell.set_door(opposite,door)
    def clear(self):
        for column in range(self.columns):
            for row in range(self.rows):
                self.cells[column][row].clear()
    def generate(self):
        self.clear()
        cell = self.cells[random.randint(0,self.columns-1)][random.randint(0,self.rows-1)]
        cell.state="inside"
        frontier = []
        cell.frontierizeNeighbors(frontier)
        while len(frontier)>0:
            index = random.randint(0,len(frontier)-1)
            cell = frontier[index]
            frontier.pop(index)
            options=[]
            for direction in cell.neighbors:
                next_cell=cell.neighbors[direction]
                if next_cell.state=="inside":
                    options.append(direction)
            direction = random.sample(options,1)[0]
            cell.doors[direction].open=True
            cell.state="inside"
            cell.frontierizeNeighbors(frontier)    
    