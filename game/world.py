import random
import math
import sys
import pdg.generator
class Avatar:
    def __init__(self):
        self.x=0
        self.y=0
        self.heading=0
        self.speed=0
        self.docked_at=None

class Island:
    def __init__(self,x,y):
        self.x=x
        self.y=y
        self.name=""
        self.visits=None
    def get_distance(self,x,y):
        delta_x = x - self.x
        delta_y = y - self.y
        return math.sqrt((delta_x)*(delta_x)+(delta_y)*(delta_y))
        

class World:
    def __init__(self):
        world_width = 100.
        world_height = 100.
        minimum_distance = 10.
        maximum_tries = 500

        self.islands=[]
        current_try = 0
        while current_try<maximum_tries:
            x = random.random() * world_width
            y = random.random() * world_height
            found = False
            for island in self.islands:
                if island.get_distance(x,y) <minimum_distance:
                    found = True
                    break
            if found:
                current_try+=1
            else:
                current_try=0
                self.islands.append(Island(x,y))

        names=[]
        while len(names)<len(self.islands):
            name = generate_island_name()
            found = False
            for existing in names:
                if existing==name:
                    found = True
                    break
            if not found:
                self.islands[len(names)].name=name
                names.append(name)

        index=0
        x = world_width/2.
        y = world_height/2.
        minimum_distance = self.islands[0].get_distance(x,y)
        current_index=0
        for island in self.islands:
            distance = island.get_distance(x,y)
            if distance<minimum_distance:
                index=current_index
                minimum_distance = distance
            current_index+=1
        self.avatar = Avatar()
        self.avatar.docked_at=index
        self.avatar.x=self.islands[index].x
        self.avatar.y=self.islands[index].y
        self.islands[index].visits=1

def generate_island_name():
    length = random.randint(1,3) + random.randint(1,3) + random.randint(1,3)
    vowel = random.randint(0,1)
    name =""
    vowels={"a":1,"e":1,"i":1,"o":1,"u":1}
    consonants={"h":1,"k":1,"l":1,"m":1,"p":1}
    while length>0:
        if vowel>0:
            name += pdg.generator.generate(vowels)
        else:
            name += pdg.generator.generate(consonants)
        vowel = 1-vowel
        length-=1
    return name

def undock(world):
    avatar = world.avatar
    if avatar.docked_at!=None:
        island = world.islands[avatar.docked_at]
        avatar.x = island.x
        avatar.y=island.y
        avatar.docked_at=None

def to_dms(radians):
    value = math.degrees(radians)
    while value<0.:
        value += 360.
    while value>=360.:
        value -= 360
    whole_degrees = math.floor(value)
    value -= whole_degrees
    value *= 60.
    whole_minutes = math.floor(value)
    value -= whole_minutes
    value *= 60.
    return (whole_degrees, whole_minutes,value)

