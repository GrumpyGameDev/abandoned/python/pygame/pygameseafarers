import pdg.statemachine, pygame
class State(pdg.statemachine.CommonState):
    def __init__(self,screen,context):
        pdg.statemachine.CommonState.__init__(self,context)
        self.screen=screen
    def draw(self):
        self.screen.fill(self.context.background_color)
        y = 0
        self.context.draw_string(self.screen,0,y,'How To Play',self.context.title_color,None)
    def handle(self,event):
        if event.type==pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                return "mainmenu"
        return "instructions"

