import pdg.statemachine, pygame
class State(pdg.statemachine.CommonState):
    items = ['Play','How To Play','Options','About','Quit']
    item_index=0

    def __init__(self,screen,context):
        pdg.statemachine.CommonState.__init__(self,context)
        self.screen=screen
    def draw(self):
        self.screen.fill(self.context.background_color)
        y = 0
        self.context.draw_string(self.screen,0,y,'Main Menu',self.context.title_color,None)
        y += 1
        index = 0
        for item in self.items:
            if index==self.item_index:
                self.context.draw_string(self.screen,0,y,item,self.context.active_color,None)                
            else:
                self.context.draw_string(self.screen,0,y,item,self.context.inactive_color,None)                
            index += 1
            y += 1

    def handle(self,event):
        if event.type==pygame.KEYDOWN:
            if event.key==pygame.K_UP:
                self.item_index = (self.item_index+len(self.items)-1) % len(self.items)
            elif event.key == pygame.K_DOWN:
                self.item_index = (self.item_index+1) % len(self.items)
            elif event.key == pygame.K_ESCAPE:
                return "confirmquit"
            elif event.key == pygame.K_SPACE:
                if self.item_index==0:#play
                    self.context.create_world()
                    return "play"
                elif self.item_index==1:#how to play
                    return "instructions"
                elif self.item_index==2:#options
                    return "options"
                elif self.item_index==3:#about
                    return "about"
                elif self.item_index==4:#quit
                    return "confirmquit"
        return "mainmenu"

