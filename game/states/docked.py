import pdg.statemachine
import pygame
import game.world

class State(pdg.statemachine.CommonState):
    items = ['Undock','Quit']
    item_index=0
    def __init__(self,screen,context):
        pdg.statemachine.CommonState.__init__(self,context)
        self.screen=screen
    def draw(self):
        self.screen.fill(self.context.background_color)
        y = 0
        avatar = self.context.world.avatar
        island = self.context.world.islands[avatar.docked_at]
        self.context.draw_string(self.screen,0,y,'Docked At: ' + island.name,self.context.title_color,None)
        y += 1
        index = 0
        for item in self.items:
            if index==self.item_index:
                self.context.draw_string(self.screen,0,y,item,self.context.active_color,None)                
            else:
                self.context.draw_string(self.screen,0,y,item,self.context.inactive_color,None)                
            index += 1
            y += 1
    def handle(self,event):
        if event.type==pygame.KEYDOWN:
            if event.key==pygame.K_UP:
                self.item_index = (self.item_index+len(self.items)-1) % len(self.items)
            elif event.key == pygame.K_DOWN:
                self.item_index = (self.item_index+1) % len(self.items)
            elif event.key == pygame.K_SPACE:
                if self.item_index==0:
                    game.world.undock(self.context.world)
                    return "play"
        return "docked"

