import pdg.statemachine
import pygame
import game.world
class State(pdg.statemachine.CommonState):
    def __init__(self,screen,context):
        pdg.statemachine.CommonState.__init__(self,context)
        self.screen=screen
    def draw(self):
        self.screen.fill(self.context.background_color)
        y = 0
        self.context.draw_string(self.screen,0,y,'At Sea',self.context.title_color,None)
        y+=1
        avatar = self.context.world.avatar
        dms = game.world.to_dms(avatar.heading)
        self.context.draw_string(self.screen,0,y,'Heading: '+str(dms[0])+'\xf8 '+str(dms[1])+'\' '+str(dms[2])+'\"',self.context.text_color,None)
    def handle(self,event):
        return "atsea"

