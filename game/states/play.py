import pdg.statemachine, pygame
class State(pdg.statemachine.CommonState):
    def __init__(self,screen,context):
        pdg.statemachine.CommonState.__init__(self,context)
        self.screen=screen
    def draw(self):
        return
    def handle(self,event):
        avatar = self.context.world.avatar
        if avatar.docked_at==None:
            return "atsea"
        else:
            return "docked"

