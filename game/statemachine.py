import pdg.statemachine
import pygame
import game.context
import game.states.mainmenu
import game.states.confirmquit
import game.states.instructions
import game.states.about
import game.states.options
import game.states.play
import game.states.docked
import game.states.atsea
def initialize(zoom):
    pygame.init()
    size = width, height = 320 * zoom, 180 * zoom
    pygame.display.set_caption('Island Interloper')
    return pygame.display.set_mode(size)

class StateMachine(pdg.statemachine.Machine):
    def __init__(self):
        screen = initialize(1)

        context = game.context.Context()

        states = {
            "mainmenu":game.states.mainmenu.State(screen,context),
            "confirmquit":game.states.confirmquit.State(screen,context),
            "about":game.states.about.State(screen,context),
            "instructions":game.states.instructions.State(screen,context),
            "options":game.states.options.State(screen,context),
            "play":game.states.play.State(screen,context),
            "docked":game.states.docked.State(screen,context),
            "atsea":game.states.atsea.State(screen,context)
        }

        pdg.statemachine.Machine.__init__(self,"mainmenu",states)
