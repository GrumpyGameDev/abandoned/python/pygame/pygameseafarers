import pygame, game.world
class Context:
    background_color  =  0,0,0
    title_color = 0,0,255
    text_color = 192,192,192
    active_color=255,255,0
    inactive_color=255,255,255
    world=None
    def draw_character(self,screen,column,row,character,foreground,background):
        self.cell.fill(foreground)
        self.cell.blit(self.romfont,(0,0), self.areas[character])
        screen_x = column * self.cell_width
        screen_y = row * self.cell_height
        if background!=None:
            screen.fill(background,pygame.Rect(screen_x,screen_y,self.cell_width,self.cell_height))
        screen.blit(self.cell,(screen_x,screen_y))
    def draw_string(self,screen,column,row,string,foreground,background):
        for character in string:
            self.draw_character(screen,column,row,ord(character),foreground,background)
            column += 1
    def create_world(self):
        self.world = game.world.World()
    def __init__(self):
        self.romfont=pygame.image.load('romfont8x8.bmp')
        width,height = self.romfont.get_size()
        self.cell_width = width//16
        self.cell_height=height//16
        self.romfont.set_colorkey((255,255,255))
        self.cell = pygame.Surface((self.cell_width,self.cell_height))
        self.cell.set_colorkey((0,0,0))
        self.areas = []
        for row in range(16):
            for column in range(16):
                self.areas.append(pygame.Rect(column*self.cell_width,row*self.cell_height,self.cell_width,self.cell_height))
